import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'Epic Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 1;

  void CallContact() {
    print("Calling contact");
  }

  void AddInfoToContact() {
    print("Adding Info To Contact");
  }

  void ItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: DefaultTabController(
            length: 3,
            child: Scaffold(
                appBar: AppBar(
                    bottom: TabBar(tabs: [
                      Tab(icon: Icon(Icons.directions_railway)),
                      Tab(icon: Icon(Icons.directions_subway)),
                      Tab(icon: Icon(Icons.directions_bike)),
                    ]),
                    title: Text("Tab Bar")),
                body: TabBarView(children: [
                  Icon(Icons.directions_railway),
                  Icon(Icons.directions_subway),
                  Icon(Icons.directions_bike),
                ]))));
  }
}
